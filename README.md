# LVDatePicker

[![CI Status](http://img.shields.io/travis/Lisnic_Victor/LVDatePicker.svg?style=flat)](https://travis-ci.org/Lisnic_Victor/LVDatePicker)
[![Version](https://img.shields.io/cocoapods/v/LVDatePicker.svg?style=flat)](http://cocoapods.org/pods/LVDatePicker)
[![License](https://img.shields.io/cocoapods/l/LVDatePicker.svg?style=flat)](http://cocoapods.org/pods/LVDatePicker)
[![Platform](https://img.shields.io/cocoapods/p/LVDatePicker.svg?style=flat)](http://cocoapods.org/pods/LVDatePicker)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

LVDatePicker is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod "LVDatePicker"
```

## Author

Lisnic_Victor, gerasim.kisslin@gmail.com

## License

LVDatePicker is available under the MIT license. See the LICENSE file for more info.
