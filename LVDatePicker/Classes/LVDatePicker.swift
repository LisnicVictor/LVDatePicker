 //
//  LVDatePicker.swift
//  Pods
//
//  Created by Victor Lisnic on 8/15/17.
//
//

import UIKit

//color
//font
//selected/disabled
//center layer customization

enum LVDatePickerComponents : Int {
    case day
    case month
    case year

    init(with int:Int) {
        switch int {
        case 0:
            self = .day
        case 1:
            self = .month
        default:
            self = .year
        }
    }
}

struct LVDatePickerViewPickers {
    var dayPicker = LVPickerView()
    var monthPicker = LVPickerView()
    var yearPicker = LVPickerView()

    var delegate : LVPickerDelegate? {
        didSet {
            dayPicker.lv_delegate = delegate
            monthPicker.lv_delegate = delegate
            yearPicker.lv_delegate = delegate
        }
    }

    var dataSource : UIPickerViewDataSource? {
        didSet {
            dayPicker.dataSource = dataSource
            monthPicker.dataSource = dataSource
            yearPicker.dataSource = dataSource
        }
    }

    var lineHeight : CGFloat = 1 {
        didSet {
            dayPicker.lineHeight = lineHeight
            monthPicker.lineHeight = lineHeight
            yearPicker.lineHeight = lineHeight
        }
    }

    var lineColor : UIColor = .black {
        didSet {
            dayPicker.lineColor = lineColor
            monthPicker.lineColor = lineColor
            yearPicker.lineColor = lineColor
        }
    }

    var lineWidthMultiplier : CGFloat = 1 {
        didSet {
            dayPicker.lineWidthRatio = lineWidthMultiplier
            monthPicker.lineWidthRatio = lineWidthMultiplier
            yearPicker.lineWidthRatio = lineWidthMultiplier
        }
    }

    var animatable : Bool = true {
        didSet {
            dayPicker.animatedRowHide = animatable
            monthPicker.animatedRowHide = animatable
            yearPicker.animatedRowHide = animatable
        }
    }

    var rowShowDuration : Double = 0.3 {
        didSet {
            dayPicker.revealAnimationDuration = rowShowDuration
            monthPicker.revealAnimationDuration = rowShowDuration
            yearPicker.revealAnimationDuration = rowShowDuration
        }
    }

    func componentFor(picker:LVPickerView) -> LVDatePickerComponents {
        switch picker {
        case dayPicker:
            return .day
        case monthPicker:
            return .month
        case yearPicker:
            return .year
        default:
            return .year
        }
    }

    func reloadComponents(for component:LVDatePickerComponents) {
        switch component {
        case .day:
            dayPicker.reloadAllComponents()
        case .month:
            monthPicker.reloadAllComponents()
        case .year:
            yearPicker.reloadAllComponents()
        }
    }

    func reloadAllComponents() {
        dayPicker.reloadAllComponents()
        monthPicker.reloadAllComponents()
        yearPicker.reloadAllComponents()
    }
}

 //Geters
public extension LVDatePicker {
    public func font(for component:Int) -> UIFont {
        return fonts[component] ?? defaultTextFont
    }

    public func textColor(for component:Int) -> UIColor {
        return  textColors[component] ?? textColor
    }

    public func disabledTextColor(for component:Int) -> UIColor {
        return  disabledTextColors[component] ?? disabledTextColor
    }

    public func textAlignment(for component:Int) -> NSTextAlignment {
        return textAlignments[component] ?? textAlignment
    }
}

//Setters
public extension LVDatePicker {
    public func setFont(_ font:UIFont, forComponent:Int) {
        fonts[forComponent] = font
        pickers.reloadComponents(for: LVDatePickerComponents(with:forComponent))
    }

    public func setTextColor(_ color:UIColor, forComponent:Int) {
        textColors[forComponent] = color
        pickers.reloadComponents( for: LVDatePickerComponents(with:forComponent))
    }

    public func setDisabledTextColor(_ color:UIColor, forComponent:Int) {
        disabledTextColors[forComponent] = color
        pickers.reloadComponents(for: LVDatePickerComponents(with:forComponent))
    }

    public func setTextAlignment(_ alignment:NSTextAlignment, forComponent:Int) {
        textAlignments[forComponent] = alignment
        pickers.reloadComponents(for: LVDatePickerComponents(with:forComponent))
    }
}

@IBDesignable
public class LVDatePicker : UIControl {

    fileprivate var pickers : LVDatePickerViewPickers = LVDatePickerViewPickers()

    @IBInspectable public var fontSize : CGFloat  {
        get {
            return defaultTextFont.pointSize
        }
        set {
            UIFont(name: textFontName, size: newValue).flatMap{
                defaultTextFont = $0
                pickers.reloadAllComponents()
            }
        }
    }

    @IBInspectable public var textFontName : String {
        get {
            return defaultTextFont.fontName
        }
        set {
            UIFont(name: newValue, size: fontSize).flatMap{
                defaultTextFont = $0
                pickers.reloadAllComponents()
            }
        }
    }

    public var defaultTextFont : UIFont = UIFont.systemFont(ofSize: 14)

    public var textAlignment : NSTextAlignment = .center
    @IBInspectable public var textColor : UIColor = .darkText
    @IBInspectable public var disabledTextColor : UIColor = .lightText

    @IBInspectable public var lineColor : UIColor {
        set{
            pickers.lineColor = newValue
        }
        get {
            return pickers.lineColor
        }
    }

    @IBInspectable public var lineHeight : CGFloat {
        set{
            pickers.lineHeight = newValue
        }
        get {
            return pickers.lineHeight
        }
    }

    @IBInspectable public var lineRatio : CGFloat {
        set{
            pickers.lineWidthMultiplier = newValue
        }
        get {
            return pickers.lineWidthMultiplier
        }
    }

    @IBInspectable public var animatable : Bool {
        set{
            pickers.animatable = newValue
        }
        get {
            return pickers.animatable
        }
    }

    @IBInspectable public var rowShowDuration : Double {
        set{
            pickers.rowShowDuration = newValue
        }
        get {
            return pickers.rowShowDuration
        }
    }

    fileprivate var textColors : [Int:UIColor] = [:]
    fileprivate var disabledTextColors : [Int:UIColor] = [:]
    fileprivate var fonts : [Int:UIFont] = [:]
    fileprivate var textAlignments : [Int:NSTextAlignment] = [:]

    public var minimumDate : Date?
    public var maximumDate : Date?

    public var date : Date = Date() {
        didSet {
            setDate(date: date)
        }
    }

    var months : [String] { return Calendar.current.monthSymbols }

    override public init(frame: CGRect) {
        super.init(frame: frame)
    }

    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    override public func prepareForInterfaceBuilder() {
        super.prepareForInterfaceBuilder()
    }

    open override func didMoveToWindow() {
        guard window != nil else {return}
        setup()
    }

    func setup() {
        pickers.delegate = self
        pickers.dataSource = self


        let stackView = UIStackView(arrangedSubviews: [pickers.dayPicker,pickers.monthPicker,pickers.yearPicker])
        stackView.alignment = .fill
        stackView.axis = .horizontal
        stackView.distribution = .fillEqually

        stackView.autoresizingMask = [.flexibleHeight,.flexibleWidth]
        stackView.frame = bounds
        addSubview(stackView)
        setDate(date: date)
    }

    func setDate(date:Date){
        pickers.dayPicker.selectRow(date.day - 1, inComponent: 0, animated: true)
        pickers.monthPicker.selectRow(date.month - 1, inComponent: 0, animated: true)
        pickers.yearPicker.selectRow(date.year - 1, inComponent: 0, animated: true)
        pickers.reloadAllComponents()
        sendActions(for: .valueChanged)
    }
}

extension LVDatePicker : UIPickerViewDataSource {
    public func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        switch pickerView {
        case pickers.dayPicker, pickers.yearPicker:
            return "\(row + 1)"
        case pickers.monthPicker:
            return months[row]
        default:
            break;
        }
        return nil
    }

    public func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        switch pickerView {
        case pickers.dayPicker:
            return Calendar.current.maximumRange(of: .day)!.count
        case pickers.monthPicker:
            return Calendar.current.maximumRange(of: .month)!.count
        case pickers.yearPicker:
            return Calendar.current.maximumRange(of: .year)!.count
        default:
            break;
        }
        return 0
    }

    public func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
}

extension LVDatePicker : LVPickerDelegate {
    public func lv_pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        guard let pickerView = pickerView as? LVPickerView else {return UIView()}
        let component = pickers.componentFor(picker: pickerView)
        let label = (view as? UILabel) ?? UILabel()
        var color = textColor(for: component.rawValue)
        let value = row+1

        if minimumDate != nil || maximumDate != nil {
            var year = pickers.yearPicker.selectedRow(inComponent: 0) + 1
            var month = pickers.monthPicker.selectedRow(inComponent: 0) + 1
            var day = pickers.dayPicker.selectedRow(inComponent: 0) + 1

            var minDate : Date?
            var maxDate : Date?

            switch pickerView {
            case pickers.dayPicker:
                day = value
                minDate = minimumDate.flatMap{ Calendar.current.date(from:DateComponents(year: $0.year, month: $0.month, day:$0.day)) }
                maxDate = maximumDate.flatMap{ Calendar.current.date(from:DateComponents(year: $0.year, month: $0.month, day:$0.day)) }
            case pickers.monthPicker:
                month = value
                minDate = minimumDate.flatMap{ Calendar.current.date(from:DateComponents(year: $0.year, month: $0.month, day:1)) }
                maxDate = maximumDate.flatMap{ Calendar.current.date(from:DateComponents(year: $0.year, month: $0.month, day:31)) }
            case pickers.yearPicker:
                year = value
                minDate = minimumDate.flatMap{ Calendar.current.date(from:DateComponents(year: $0.year, month: 1, day:1)) }
                maxDate = maximumDate.flatMap{ Calendar.current.date(from:DateComponents(year: $0.year, month: 12, day:31)) }
            default:
                break
            }

            let resultDate = Calendar.current.date(from:DateComponents(year: year, month: month, day:day))!

            minDate.flatMap{
                if resultDate < $0 {
                    color = disabledTextColor(for: component.rawValue)
                }
            }

            maxDate.flatMap{
                if resultDate > $0 {
                    color = disabledTextColor(for: component.rawValue)
                }
            }
        }

        label.text = self.pickerView(pickerView, titleForRow: row, forComponent:0)
        label.textColor = color
        label.textAlignment = textAlignment(for: component.rawValue)
        label.font = font(for: component.rawValue)
        label.adjustsFontSizeToFitWidth = true
        
        return label

    }

    public func lv_pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        let value = row + 1

        var year = pickers.yearPicker.selectedRow(inComponent: 0) + 1
        var month = pickers.monthPicker.selectedRow(inComponent: 0) + 1
        var day = pickers.dayPicker.selectedRow(inComponent: 0) + 1

        switch pickerView {
        case pickers.dayPicker:
            day = value
        case pickers.monthPicker:
            month = value
        case pickers.yearPicker:
            year = value
        default:
            break
        }

        let maxDay = DateHelpers.getNumberOfDays(of: month, year: year)
        day = day <= maxDay ? day : maxDay

        var resultDate = Calendar.current.date(from:DateComponents(year: year, month: month, day:day))!

        minimumDate.flatMap{
            if resultDate < $0 {
                resultDate = $0
            }
        }

        maximumDate.flatMap{
            if resultDate > $0 {
                resultDate = $0
            }
        }
        
        date = resultDate
    }
}

extension Date {
    var year : Int {
        let datecomponents = Calendar.current.dateComponents(in: TimeZone.current, from: self)
        guard let year = datecomponents.year else {return 0}
        return year
    }

    var month : Int {
        let datecomponents = Calendar.current.dateComponents(in: TimeZone.current, from: self)
        guard let month = datecomponents.month else {return 0}
        return month
    }

    var day : Int {
        let datecomponents = Calendar.current.dateComponents(in: TimeZone.current, from: self)
        guard let day = datecomponents.day else {return 0}
        return day
    }
}

struct DateHelpers {
    static func getNumberOfDays(of month:Int,year:Int, calendar:Calendar = Calendar.current) -> Int {
        let dateComponents = DateComponents(year: year, month: month)
        let calendar = Calendar.current
        let date = calendar.date(from: dateComponents)!

        let range = calendar.range(of: .day, in: .month, for: date)!
        return range.count
    }
}

