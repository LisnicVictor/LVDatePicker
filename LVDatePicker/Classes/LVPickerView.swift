    //
//  LVPickerView.swift
//  Pods
//
//  Created by Victor Lisnic on 8/23/17.
//
//

import UIKit

public protocol LVPickerDelegate : class {
    func lv_pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView
    func lv_pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int)
    func lv_pickerView(_ pickerView: UIPickerView, rowHeightForComponent component: Int) -> CGFloat?
    func lv_pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String?
    func lv_pickerView(_ pickerView: UIPickerView, attributedTitleForRow row: Int, forComponent component: Int) -> NSAttributedString?
}

public extension LVPickerDelegate {
    func lv_pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {}
    func lv_pickerView(_ pickerView: UIPickerView, rowHeightForComponent component: Int) -> CGFloat? {return nil}
    func lv_pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {return nil}
    func lv_pickerView(_ pickerView: UIPickerView, attributedTitleForRow row: Int, forComponent component: Int) -> NSAttributedString? {return nil}
}

@IBDesignable
open class LVPickerView : UIPickerView , UIPickerViewDelegate {

    @IBInspectable public var lineWidthRatio : CGFloat = 1.0
    @IBInspectable public var lineHeight : CGFloat = 1.0
    @IBInspectable public var lineColor : UIColor = .black
    @IBInspectable public var animatedRowHide : Bool = false

    @IBOutlet public var _delegate : AnyObject? {
        set {
            lv_delegate = newValue as? LVPickerDelegate
        }
        get {
            return lv_delegate
        }
    }

    public var revealAnimationDuration : Double = 0.3

    weak var lv_delegate : LVPickerDelegate?

    private var isActive : Bool = false

    fileprivate var showElements : Bool = false {
        didSet {
            //            if oldValue != showElements {
            DispatchQueue.main.async { [unowned self] in
                self.reloadAllComponents()
                //            }
            }
        }
    }

    fileprivate var gestureY : CGFloat = 0

    fileprivate var bottomLine : UIView?
    fileprivate var topLine : UIView?

    private lazy var _mask : CALayer = {
        let layer = CALayer()
        self.layer.mask = layer
        layer.backgroundColor = UIColor.black.cgColor
        return layer
    }()

    private lazy var recognizer : UIGestureRecognizer = {

        let recognizer = UILongPressGestureRecognizer(target: self, action: #selector(recognizer(gestureRecognizer:)))
        recognizer.minimumPressDuration = 0.0
        recognizer.allowableMovement = 10000
        recognizer.delegate = self
        return recognizer
    }()

    open override func prepareForInterfaceBuilder() {
        super.prepareForInterfaceBuilder()
    }

    open override func didMoveToWindow() {
        super.didMoveToWindow()
        if window != nil {
            delegate = self
            addGestureRecognizer(recognizer)
            clipsToBounds = true
        }
    }

    @objc func recognizer(gestureRecognizer: UIGestureRecognizer) {
        switch gestureRecognizer.state {
        case .began:
//            elements(show:true)
            gestureY = gestureRecognizer.location(in: self).y

            guard
            let maxY : CGFloat = bottomLine?.frame.origin.y,
            let minY : CGFloat = topLine?.frame.origin.y
                else {return}

            if gestureY > minY && gestureY < maxY {
                elements(show: true)
            }
        case .cancelled,.ended,.failed:
            let newY = gestureRecognizer.location(in: self).y
            let delta = abs(Int(newY) - Int(gestureY))
            print(delta)
            if delta < 10 {
                elements(show:false)
            }
        default:
            break;
        }
    }

    open override func layoutSubviews() {
        super.layoutSubviews()
        setLines()
    }

    func setLines() {
        let _ = subviews.map{
            if $0.bounds.height < 1 {
                let y = $0.frame.origin.y
                if y > bounds.height/2 {
                    bottomLine = $0
                }else {
                    topLine = $0
                    $0.backgroundColor = .clear
                }
            }
        }

        setLine(show:!isActive)
        setMask(show:isActive)
    }


    func setMask(show:Bool) {
        guard let top = topLine, let bottom = bottomLine else {return}

        let height = show ? bounds.height : abs((top.frame.origin.y - bottom.frame.origin.y )) + lineHeight
        let x = bounds.origin.x
        let y = show ? bounds.origin.y : top.frame.origin.y
        _mask.frame = CGRect(x:x, y:y, width: bounds.width, height: height)

    }

    func setLine(show:Bool) {
        guard let line = bottomLine else {return}
        line.backgroundColor = lineColor

        line.frame.origin.x = show ? (self.bounds.width - self.bounds.width * self.lineWidthRatio) / 2 : self.bounds.width/2
        line.frame.origin.y = line.frame.origin.y
        line.frame.size.width = show ? self.bounds.width * self.lineWidthRatio : 0
        line.frame.size.height = lineHeight
    }

    public func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        elements(show: false)
        lv_delegate?.lv_pickerView(pickerView, didSelectRow: row, inComponent: component)
    }

    open  func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        let view = lv_delegate?.lv_pickerView(pickerView, viewForRow: row, forComponent: component, reusing: view) ?? UIView()
        view.isUserInteractionEnabled = false
        if !showElements {
            if row != selectedRow(inComponent: component) {
                view.alpha = 0
            }
        } else {
            view.alpha = 1
        }

        return view
    }

    open override func selectRow(_ row: Int, inComponent component: Int, animated: Bool) {
        super.selectRow(row, inComponent: component, animated: animated)
        elements(show: false)
    }

    public func pickerView(_ pickerView: UIPickerView, rowHeightForComponent component: Int) -> CGFloat {
        return lv_delegate?.lv_pickerView(pickerView, rowHeightForComponent: component) ?? 30
    }

    public func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return lv_delegate?.lv_pickerView(pickerView,titleForRow:row,forComponent:component)
    }

    public func pickerView(_ pickerView: UIPickerView, attributedTitleForRow row: Int, forComponent component: Int) -> NSAttributedString? {
        return lv_delegate?.lv_pickerView(pickerView,attributedTitleForRow:row,forComponent:component)
    }
    func elements(show:Bool) {

        guard let _ = bottomLine else {return}
        isActive = show
        if show {
            showElements = true
        }
        UIView.animate(withDuration: revealAnimationDuration, animations: { [unowned self] in
            self.setLine(show:!show)
            CATransaction.setAnimationDuration(self.revealAnimationDuration)
            self.setMask(show:show)
        }) { [unowned self] _ in
            if show == false{
                self.showElements = show
            }
        }
    }
}

extension LVPickerView : UIGestureRecognizerDelegate {
    public func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {

        let gestureY = gestureRecognizer.location(in: self).y

        guard
            let maxY : CGFloat = bottomLine?.frame.origin.y,
            let minY : CGFloat = topLine?.frame.origin.y
            else {return false}

        if showElements || gestureY > minY && gestureY < maxY {
            return true
        }

        return false
    }
}
