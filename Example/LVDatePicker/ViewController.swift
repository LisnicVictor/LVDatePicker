//
//  ViewController.swift
//  LVDatePicker
//
//  Created by Lisnic_Victor on 08/15/2017.
//  Copyright (c) 2017 Lisnic_Victor. All rights reserved.
//

import UIKit
import LVDatePicker

class ViewController: UIViewController {

    @IBOutlet weak var picker : LVDatePicker!

    override func viewDidLoad() {
        super.viewDidLoad()
        picker.disabledTextColor = .gray
        picker.maximumDate = Date()
    }
    
}
